# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>
#
# ##### END GPL LICENSE BLOCK #####

from setuptools import setup, find_packages


__author__ = "Lothar Krause"
__maintainer__ = "Sergi Blanch-Torné, Emilio Morales"
__email__ = "sblanch@cells.es, emorales@cells.es"
__copyright__ = "Copyright 2022, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

__project__ = "Danfysik"
__description__ = (
    "Python module to control via Tango-CS the Danfysik " "power converter."
)
__longDesc__ = """
This module has been prepared to provide control using the Tango-CS of the
power converter from Danfysik manufacturer.
"""
__url__ = "https://gitlab.com/tango-controls/device-servers/DeviceClasses/powersupply/Danfysik_9100"

__version__ = "__version__ = '2.0.0'"


setup(
    name=__project__,
    license=__license__,
    description=__description__,
    long_description=__longDesc__,
    version=__version__,
    author=__author__,
    author_email=__email__,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: "
        "GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Topic :: Scientific/Engineering :: "
        "Interface Engine/Protocol Translator",
        "Topic :: Software Development :: Embedded Systems",
        "Topic :: Software Development :: Libraries :: " "Python Modules",
        "Topic :: System :: Hardware",
    ],
    packages=find_packages(),
    url=__url__,
    entry_points={"console_scripts": ["Danfysik = Danfysik.Danfysik:main"]},
)

# for the classifiers review see:
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
#
# Development Status :: 1 - Planning
# Development Status :: 2 - Pre-Alpha
# Development Status :: 3 - Alpha
# Development Status :: 4 - Beta
# Development Status :: 5 - Production/Stable
