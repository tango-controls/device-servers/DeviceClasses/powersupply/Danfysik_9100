# -*- coding:utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Lothar Krause"
__maintainer__ = "Sergi Blanch-Torné, Emilio Morales"
__email__ = "emorales@cells.es"
__copyright__ = "Copyright 2022, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


import sys

import time
import traceback
import threading

try:
    import tango
except Exception as e:
    print(("Importing PyTango as tango: {}".format(e)))
    import PyTango as tango


from . import state

VOLTAGE_READBACK_REGISTER_AD = 2
VOLTAGE_READBACK_FACTOR = 10

CURRENT_READBACK_REGISTER_AD = 8
CURRENT_READBACK_FACTOR = 100

CURRENT_SETPOINT_REGISTER_AD = 19
CURRENT_SETPOINT_FACTOR = 1000

AMBIENT_TEMPERATURE_REGISTER_AD = 1
AMBIENT_TEMPERATURE_FACTOR = 1


# ==================================================================
#   Danfysik Class Description:
#
#   The tailor is rich and my mother is in the kitchen.
#
# ==================================================================
class Danfysik(tango.LatestDeviceImpl):
    # --------- Add you global variables here --------------------------

    # ------------------------------------------------------------------
    #   Device constructor
    # ------------------------------------------------------------------
    def __init__(self, cl, name):
        tango.LatestDeviceImpl.__init__(self, cl, name)
        self._important_logs = []
        self.dp = None
        self.statusDict = state.DanfysikStatusDict
        self._errors = []
        self._remoteMode = None
        self._haveSerialLineConn = False
        self._serialLineTrace = []
        Danfysik.init_device(self)

    # ------------------------------------------------------------------
    #   Device destructor
    # ------------------------------------------------------------------
    def delete_device(self):
        self.info_stream(
            "[Device delete_device method] for device %s" % (self.get_name())
        )

    # ------------------------------------------------------------------
    #   Device initialization
    # ------------------------------------------------------------------
    def init_device(self):
        self.debug_stream("In %s::init_device()" % (self.get_name()))
        self.changeState(tango.DevState.INIT)
        self.get_device_properties(self.get_device_class())

        self.info_stream(
            "Serial lines is %s (%s)"
            % (self.SerialLine, type(self.SerialLine))
        )
        self._linkSerial()

        self.set_change_event("State", True, False)
        self.set_change_event("Status", True, False)
        #        self.set_change_event('Current', True, False)
        #        self.set_change_event('Voltage', True, False)
        self.set_change_event("RemoteMode", True, False)
        self.set_change_event("Errors", True, False)
        self.set_change_event("SerialLineTrace", True, False)

    def _linkSerial(self):
        try:
            if self.dp is None:
                self.dp = tango.DeviceProxy(self.SerialLine)
            self.dp.DevSerWriteString("S1\r")
            self.waiting = threading.Event()
            self.waiting.wait(0.1)
            if self.dp is not None:
                self.statusString = self.dp.DevSerReadRaw().strip("\n\r")
                if self.statusString and self.statusString[0] == "":
                    self.changeState(tango.DevState.FAULT)
                    self.cleanAllImportantLogs()
                    self.addStatusMsg("Serial line is not responding", False)
                    if self._haveSerialLineConn:
                        self.append2SerialLineTrace(
                            "Serial line is not responding"
                        )
                        self._haveSerialLineConn = False
                elif self.statusString and self.statusString[0] == "!":
                    self.changeState(tango.DevState.OFF)
                    if not self._haveSerialLineConn:
                        self.append2SerialLineTrace("Serial line recovered")
                        self._haveSerialLineConn = True
                else:
                    self.changeState(tango.DevState.ON)
                    if not self._haveSerialLineConn:
                        self.append2SerialLineTrace("Serial line recovered")
                        self._haveSerialLineConn = True
                        self.dp.DevSerWriteString("ERRT\r")
                        self.dp.DevSerFlush(2)
            else:
                self.changeState(tango.DevState.FAULT)
                self.cleanAllImportantLogs()
                msg = "Cannot link with serial line"
                self.addStatusMsg(msg, True)
        except Exception as e:
            self.dp = None
            self.changeState(tango.DevState.FAULT)
            self.cleanAllImportantLogs()
            msg = "Exception when try to link with serial line"
            self.addStatusMsg(msg, True)
            self.debug_stream(msg + ": %s" % (e))
            traceback.format_exc()

    # ------------------------------------------------------------------
    #   Always excuted hook method
    # ------------------------------------------------------------------
    def always_executed_hook(self):
        self.debug_stream("In %s::always_excuted_hook()" % (self.get_name()))

    # ==================================================================
    #
    #   Danfysik read/write attribute methods
    #
    # ==================================================================
    # ------------------------------------------------------------------
    #   Read Attribute Hardware
    # ------------------------------------------------------------------
    def read_attr_hardware(self, data):
        self.debug_stream("In %s::read_attr_hardware()" % (self.get_name()))

    # ------------------------------------------------------------------
    #   Read Current attribute
    # ------------------------------------------------------------------
    def read_Current(self, attr):
        self.debug_stream("In %s::read_Current()" % (self.get_name()))
        #   Add your own code here
        self.dp.DevSerWriteString("AD %d\r" % CURRENT_READBACK_REGISTER_AD)
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            attr_Current_read = float(res) / CURRENT_READBACK_FACTOR
            attr.set_value(attr_Current_read)
        else:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)

    def is_Current_allowed(self, req_type):
        if self.get_state() in [tango.DevState.INIT] or self.dp is None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

    # ------------------------------------------------------------------
    #   Read StateCode64 attribute
    # ------------------------------------------------------------------
    def read_StateCode64(self, attr=None):
        self.debug_stream("In %s::read_StateCode64()" % (self.get_name()))

        if self.get_state() == tango.DevState.FAULT and self.dp is None:
            # TODO: distinguish between communication fault and PS fault
            if attr is not None:
                attr.set_quality(tango.AttrQuality.ATTR_INVALID)
            return

        self.dp.DevSerWriteString("S1\r")
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")

        if not self._IsAnswerError(res):
            stateCode64_bitstr = "".join(
                ["1" if c == "!" else "0" for c in res]
            )

            try:
                attr_StateCode64_read = int("0b" + stateCode64_bitstr, 2)
            except Exception as e:
                print("StateCode64_read will be 0: {}".format(e))
                attr_StateCode64_read = 0

            self.debug_stream(
                "StateCode64 %s = 0b%s = %d"
                % (res, stateCode64_bitstr, attr_StateCode64_read)
            )

            if attr is None:
                return attr_StateCode64_read
            else:
                attr.set_value(attr_StateCode64_read)
        elif attr is not None:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)

    # ------------------------------------------------------------------
    #   Read CurrentSetpoint attribute
    # ------------------------------------------------------------------
    def read_CurrentSetpoint(self, attr):
        self.debug_stream("In %s::read_CurrentSetpoint()" % (self.get_name()))
        # Add your own code here

        # self.dp.DevSerWriteString('RA\r')
        self.dp.DevSerWriteString("AD %d\r" % CURRENT_SETPOINT_REGISTER_AD)
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            attr_CurrentSetpoint_read = int(res)
            attr_CurrentSetpoint_read = (
                float(attr_CurrentSetpoint_read) / CURRENT_SETPOINT_FACTOR
            )
            attr.set_value(attr_CurrentSetpoint_read)
        else:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)

    # ------------------------------------------------------------------
    #   Write CurrentSetpoint attribute
    # ------------------------------------------------------------------
    def write_CurrentSetpoint(self, attr):
        self.debug_stream("In %s::write_CurrentSetpoint()" % (self.get_name()))
        #   Add your own code here

        self.CurrentSetpoint = attr.get_write_value() * CURRENT_SETPOINT_FACTOR
        self.dp.DevSerWriteString("DA 0 " + str(self.CurrentSetpoint) + "\r")
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            self.push_change_event("CurrentSetpoint", self.CurrentSetpoint)

    def is_CurrentSetpoint_allowed(self, req_type):
        if self.get_state() in [tango.DevState.INIT] or self.dp is None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

    # ------------------------------------------------------------------
    #   Read Voltage attribute
    # ------------------------------------------------------------------
    def read_Voltage(self, attr):
        self.debug_stream("In %s::read_Voltage()" % (self.get_name()))
        #   Add your own code here

        self.dp.DevSerWriteString("AD %d\r" % VOLTAGE_READBACK_REGISTER_AD)
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            attr_Voltage_read = float(res) / VOLTAGE_READBACK_FACTOR
            attr.set_value(attr_Voltage_read)
        else:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)

    def is_Voltage_allowed(self, req_type):
        if self.get_state() in [tango.DevState.INIT] or self.dp is None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

    # ------------------------------------------------------------------
    #   Read AmbientTemperature attribute
    # ------------------------------------------------------------------
    def read_AmbientTemperature(self, attr):
        self.debug_stream(
            "In %s::read_AmbientTemperature()" % (self.get_name())
        )
        #   Add your own code here

        self.dp.DevSerWriteString("AD %d\r" % AMBIENT_TEMPERATURE_REGISTER_AD)
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            attr_Voltage_read = float(res) / AMBIENT_TEMPERATURE_FACTOR
            attr.set_value(attr_Voltage_read)
        else:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)

    def is_AmbientTemperature_allowed(self, req_type):
        if self.get_state() in [tango.DevState.INIT] or self.dp is None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

    # ------------------------------------------------------------------
    #   Read CurrentSlewrate attribute
    # ------------------------------------------------------------------
    def read_CurrentSlewrate(self, attr):
        self.debug_stream("In %s::read_CurrentSlewrate()" % (self.get_name()))
        #   Add your own code here

        self.dp.DevSerWriteString("DA 1\r")
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            self.v = int(res.split(" ")[1])
            attr_CurrentSlewrate_read = float(self.v) / 1000
            attr.set_value(attr_CurrentSlewrate_read)
        else:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)

    # ------------------------------------------------------------------
    #   Write CurrentSlewrate attribute
    # ------------------------------------------------------------------
    def write_CurrentSlewrate(self, attr):
        self.debug_stream("In %s::write_CurrentSlewrate()" % (self.get_name()))
        #   Add your own code here

        self.CurrentSlewrate = attr.get_write_value() * 1000
        self.dp.DevSerWriteString("DA 1 " + str(self.CurrentSlewrate) + "\r")
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            self.push_change_event("CurrentSlewrate", self.CurrentSlewrate)

    def is_CurrentSlewrate_allowed(self, req_type):
        if self.get_state() in [tango.DevState.INIT] or self.dp is None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

    # ------------------------------------------------------------------
    #   Read RemoteMode attribute
    # ------------------------------------------------------------------
    def read_RemoteMode(self, attr=None):
        self.debug_stream("In %s::read_RemoteMode()" % (self.get_name()))
        #   Add your own code here

        if self._remoteMode is None:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)
        else:
            attr.set_value(self._remoteMode)

    #        self.dp.DevSerWriteString('CMD\r')
    #        self.waiting=threading.Event()
    #        self.waiting.wait(0.1)
    #        res = self.dp.DevSerReadRaw().strip('\n\r')
    #        if not self._IsAnswerError(res):
    #            if res=='REM ': attr_RemoteMode_read=True
    #            else: attr_RemoteMode_read=False
    #            if attr==None:
    #                return attr_RemoteMode_read
    #            else:
    #                attr.set_value(attr_RemoteMode_read)
    #        else:
    #            if attr==None:
    #                return False
    #            else:
    #                attr.set_quality(tango.AttrQuality.ATTR_INVALID)

    # ------------------------------------------------------------------
    #   Write RemoteMode attribute
    # ------------------------------------------------------------------
    def write_RemoteMode(self, attr):
        self.debug_stream("In %s::write_RemoteMode()" % (self.get_name()))
        #   Add your own code here

        self.value = attr.get_write_value()
        if self.value is True:
            self.Remote = "REM "
        else:
            self.Remote = "LOC "
        self.dp.DevSerWriteString(str(self.Remote) + "\r")
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            self.push_change_event("RemoteMode", self.value)

    def is_RemoteMode_allowed(self, req_type):
        if self.get_state() in [tango.DevState.INIT] or self.dp is None:
            #    End of Generated Code
            #    Re-Start of Generated Code
            return False
        return True

    # ------------------------------------------------------------------
    #   Read Errors attribute
    # ------------------------------------------------------------------
    def read_Errors(self, attr):
        self.debug_stream("In %s::read_Errors()" % (self.get_name()))
        #   Add your own code here

        attr.set_value(self._errors, len(self._errors))

    # ------------------------------------------------------------------
    #   Read SerialLineTrace attribute
    # ------------------------------------------------------------------
    def read_SerialLineTrace(self, attr):
        self.debug_stream("In %s::read_SerialLineTrace()" % (self.get_name()))
        #   Add your own code here

        attr.set_value(self._serialLineTrace, len(self._serialLineTrace))

    # ==================================================================
    #
    #   Danfysik command methods
    #
    # ==================================================================

    # ------------------------------------------------------------------
    #   Off command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def Off(self):
        self.debug_stream("In %s::Off()" % (self.get_name()))
        #   Add your own code here
        self.dp.DevSerWriteString("F\r")

    # ------------------------------------------------------------------
    #   On command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def On(self):
        self.debug_stream("In %s::On()" % (self.get_name()))
        # Add your own code here
        self.dp.DevSerWriteString("N\r")

    # ------------------------------------------------------------------
    #   ResetInterlocks command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def ResetInterlocks(self):
        self.debug_stream("In %s::ResetInterlocks()" % (self.get_name()))
        #  Add your own code here
        self.dp.DevSerWriteString("RS\r")
        self.cleanAllImportantLogs()

    # ------------------------------------------------------------------
    #   updateState command:
    #
    #   Description:
    # ------------------------------------------------------------------

    def _updateStatusString(self, stateCode64):
        if stateCode64 is None:
            self.statusString = ""
        else:
            self.statusString = bin(stateCode64)[2:]  # remove the '0b' header
            while len(self.statusString) < len(self.statusDict):
                self.statusString = "0" + self.statusString

    def _updateNextStatusStrings(self):
        for i in range(1, len(self.statusString)):
            if i in [0, 1, 2, 3, 5, 6, 8, 13, 15, 23]:
                continue  # avoid this positions
            if self.statusString[i] == "1":
                self._appendErrors(idx=i)

    def _appendErrors(self, idx=None):
        if idx is None:
            idx = 0

        self._errors.append(
            self.statusDict[idx][0] + " : " + self.statusDict[idx][1][0] + "."
        )

    def _updateRemoteMode(self, mode):
        if not mode == self._remoteMode:
            self._remoteMode = mode
            self.push_change_event("RemoteMode", self._remoteMode)

    def _pushErrors(self):
        if len(self._errors) > 0:
            self.push_change_event("Errors", self._errors)

    def _checkSerialLine(self):
        if self._haveSerialLineConn:
            self.append2SerialLineTrace("Serial line is not responding")
            self._haveSerialLineConn = False
        if not self._haveSerialLineConn:
            self.append2SerialLineTrace("Serial line recovered")
            self._haveSerialLineConn = True

    def UpdateStatus(self):
        self.debug_stream("In %s::UpdateStatus()" % (self.get_name()))

        if self.dp is None:
            self._linkSerial()
            self.warn_stream("Trying to relink with the serial port.")
            return

        # FIXME: this can be improved without use string conversion!
        try:
            self._errors = []
            stateCode64 = self.read_StateCode64()
            self._updateStatusString(stateCode64)

            # start with the interpretation of the first bit of the state
            if self.statusString == "":
                self.cleanAllImportantLogs()
                self.changeState(tango.DevState.FAULT)
                self.addStatusMsg("Serial line is not responding", False)
                self._checkSerialLine()

            elif len(self.statusString) > 0 and self.statusString[0] == "0":
                # self._errors.append(self.statusDict[0][0]+' : '+self.statusDict[0][1][1]+'.')
                # adjusting state of the device is necessary
                if self.get_state() != tango.DevState.ON:
                    self.changeState(tango.DevState.ON)
                self._checkSerialLine()
            else:
                self._appendErrors()
                if self.get_state() != tango.DevState.OFF:
                    self.changeState(tango.DevState.OFF)
                self._checkSerialLine()
            # when first element processed, proceed with the next ones
            self._updateNextStatusStrings()
            # Recollect the messaging
            self._pushErrors()

            mode = self.isRemoteMode()
            self._updateRemoteMode(mode)
        except Exception as e:
            self.error_stream(
                "UpdateStatus() method failed!: %s \n %s"
                % (e, traceback.format_exc())
            )

    # ------------------------------------------------------------------
    #   ReadAD command:
    #
    #   Description:
    # ------------------------------------------------------------------
    def ReadAD(self, argin):
        self.debug_stream("In %s::ReadAD(%d)" % (self.get_name(), int(argin)))
        self.dp.DevSerWriteString("AD %d\r" % int(argin))
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            return res
        else:
            return "None"

    ###
    # auxiliar methods
    def isRemoteMode(self):
        self.dp.DevSerWriteString("CMD\r")
        self.waiting = threading.Event()
        self.waiting.wait(0.1)
        res = self.dp.DevSerReadRaw().strip("\n\r")
        if not self._IsAnswerError(res):
            if res == "REM ":
                return True
            else:
                return False
        else:
            return None

    def _IsAnswerError(self, answer):
        if answer == "":
            self.warn_stream("Void answer from the Power Converter")
            return True
        if answer.startswith("?/x07"):
            error = answer.split(" ", 1)[1]
            self.error_stream("Power Converter say: %s" % (error))
            return True
        return False

    def changeState(self, newstate):
        self.debug_stream(
            "In %s::changeState(%s)" % (self.get_name(), str(newstate))
        )
        self.set_state(newstate)
        self.addStatusMsg()
        self.push_change_event("State", newstate)
        # When state changes, clean the previous non important logs.
        self.addStatusMsg("")

    def fireEventsList(self, eventsAttrList):
        timestamp = time.time()
        for attrEvent in eventsAttrList:
            try:
                self.debug_stream(
                    "In %s::fireEventsList() attribute: %s"
                    % (self.get_name(), attrEvent[0])
                )
                if len(attrEvent) == 3:  # specifies quality
                    self.push_change_event(
                        attrEvent[0], attrEvent[1], timestamp, attrEvent[2]
                    )
                else:
                    self.push_change_event(
                        attrEvent[0],
                        attrEvent[1],
                        timestamp,
                        tango.AttrQuality.ATTR_VALID,
                    )
            except Exception as e:
                self.error_stream(
                    "In %s::fireEventsList() Exception with attribute %s"
                    % (self.get_name(), attrEvent[0])
                )
                print(e)

    # @todo: clean the important logs when they loose importance.
    def cleanAllImportantLogs(self):
        self.debug_stream("In %s::cleanAllImportantLogs()" % self.get_name())
        self._important_logs = []
        self.addStatusMsg("")

    def addStatusMsg(self, current=None, important=False):
        try:
            self.info_stream("In %s::addStatusMsg()" % self.get_name())
            msg = "The device is in %s state.\n" % (self.get_state())
            for ilog in self._important_logs:
                msg = "%s%s\n" % (msg, ilog)
            if current is not None and not current == "":
                status = "%s%s\n" % (msg, current)
            else:
                status = "%s" % (msg)
            self.set_status(status)
            self.push_change_event("Status", status)
            if important and current not in self._important_logs:
                self._important_logs.append(current)
        except Exception as e:
            self.error_stream("Exception adding status message: %s" % (e))
            traceback.format_exc()

    def append2SerialLineTrace(self, msg):
        self._serialLineTrace.append("%s: %s" % (time.ctime(), msg))
        while len(self._serialLineTrace) >= 32:
            self._serialLineTrace.pop()
        self.push_change_event("SerialLineTrace", self._serialLineTrace)


# end auxiliar methods
###


# ==================================================================
#
#   DanfysikClass class definition
#
# ==================================================================
class DanfysikClass(tango.DeviceClass):
    # Class Properties
    class_property_list = {}

    #  Device Properties
    device_property_list = {
        "SerialLine": [
            tango.DevString,
            "The serial line to be used.",
            # ['alba02:10000/lab/ct/dctmoxa07-04']],#No alba's lab default value
            [""],
        ],
    }

    #  Command definitions
    cmd_list = {
        "Off": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "On": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "ResetInterlocks": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "UpdateStatus": [[tango.DevVoid, ""], [tango.DevVoid, ""]],
        "ReadAD": [[tango.DevShort, ""], [tango.DevString, ""]],
    }

    #  Attribute definitions
    attr_list = {
        "Current": [
            [tango.DevDouble, tango.SCALAR, tango.READ],
            {
                "unit": "A",
            },
        ],
        "CurrentSetpoint": [
            [tango.DevDouble, tango.SCALAR, tango.READ_WRITE],
            {
                "unit": "A",
            },
        ],
        "Voltage": [
            [tango.DevDouble, tango.SCALAR, tango.READ],
            {
                "unit": "V",
            },
        ],
        "CurrentSlewrate": [
            [tango.DevDouble, tango.SCALAR, tango.READ_WRITE],
            {
                "unit": "A/s",
            },
        ],
        "AmbientTemperature": [
            [tango.DevDouble, tango.SCALAR, tango.READ],
            {
                "unit": "Celsius",
            },
        ],
        "StateCode64": [[tango.DevLong64, tango.SCALAR, tango.READ]],
        "RemoteMode": [[tango.DevBoolean, tango.SCALAR, tango.READ_WRITE]],
        "Errors": [
            [tango.DevString, tango.SPECTRUM, tango.READ, 32],
        ],
        "SerialLineTrace": [
            [tango.DevString, tango.SPECTRUM, tango.READ, 32],
            {
                "description": "Collect traces about the serial line cuts and recovers"
            },
        ],
    }

    # ------------------------------------------------------------------
    #   DanfysikClass Constructor
    # ------------------------------------------------------------------
    def __init__(self, name):
        tango.DeviceClass.__init__(self, name)
        self.set_type(name)
        print("In DanfysikClass constructor")


# ==================================================================
#
#   Danfysik class main method
#
# ==================================================================
def main():
    try:
        py = tango.Util(sys.argv)
        py.add_TgClass(DanfysikClass, Danfysik, "Danfysik")

        U = tango.Util.instance()
        U.server_init()
        U.server_run()

    except tango.DevFailed as e:
        print(("-------> Received a DevFailed exception:", e))
    except Exception as e:
        print(("-------> An unforeseen exception occured....", e))


if __name__ == "__main__":
    main()
