# Danfysik_9100
This is a device server that provides control to Danfysik 9100 power supply.
At ALABA Synchrotron, this module was originally developed by Lothar Krause and
now is mantained by ALBA CONTROLS-CT4 group.


![license GPLv3+](https://img.shields.io/badge/license-GPLv3+-green.svg)

release 2.0.0

authors: Lothar Krause, Sergi Blanch-Torné, Emilio Morales

![](https://img.shields.io/badge/python-2.7-green.svg)
![](https://img.shields.io/badge/python-3.10-red.svg)


## Release Notes
* 1.7.1: Change Danfysik/Danfysik.py source from SVN ...trunk/new_src/Danfysik.py to .../trunk/Danfysik.py
* 1.7.0: First migration git.cells.es and first debian package build.
* 1.6.0: Latest Blissbuilder package at ALBA Synchrotron.

## Old versions
* Code migrated from [Source Forge](https://sourceforge.net/p/tango-ds/code/HEAD/tree/DeviceClasses/PowerSupply/Danfysik_9100/)